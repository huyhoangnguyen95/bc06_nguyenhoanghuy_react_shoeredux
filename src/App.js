import logo from "./logo.svg";
import "./App.css";
import Shoeshop from "./Shoeshop_Redux/Shoeshop";

function App() {
  return (
    <div className="App">
      <Shoeshop />
    </div>
  );
}

export default App;
