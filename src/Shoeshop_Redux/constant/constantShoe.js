export const GET_DETAIL = "GET_DETAIL";
export const ADD_TO_CART = "ADD_TO_CART";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const DELETE_SHOE = "DELETE_SHOE";
