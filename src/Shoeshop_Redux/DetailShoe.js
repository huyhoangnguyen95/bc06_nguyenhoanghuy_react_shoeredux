import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { name, price, description, image } = this.props.detailShoe;
    return (
      <div>
        <div className="card">
          <img
            style={{ width: 200, margin: "auto" }}
            className="card-img-top"
            src={image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">${price}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Item 1</li>
            <li className="list-group-item">Item 2</li>
            <li className="list-group-item">Item 3</li>
          </ul>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoe: state.shoe.detailShoe,
  };
};

export default connect(mapStateToProps)(DetailShoe);
