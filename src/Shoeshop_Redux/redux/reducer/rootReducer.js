import { combineReducers } from "redux";
import shoeReducer from "./shoeReducer";

export const rootReducer = combineReducers({
  shoe: shoeReducer,
});

// key : manage child reducer

//  value : child reducer
