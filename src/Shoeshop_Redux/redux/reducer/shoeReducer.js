import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  GET_DETAIL,
} from "../../constant/constantShoe";
import { shoeList } from "../../dataShoe";

const initialState = {
  shoeArr: shoeList,
  detailShoe: shoeList[0],
  cart: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_DETAIL: {
      return { ...state, detailShoe: action.payload };
    }
    case ADD_TO_CART: {
      // th1 : tim vi tri khi bam them , neu == -1 la ko tim thay , tao moi object co them key soLuong : 1 (push)
      // th2 : neu != -1 là tìm  thấy , update key soLuong (ko push)

      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].soLuong += 1;
      }
      // cloneCart.push(action.payload);
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let { id, value } = action.payload;
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == id;
      });

      cloneCart[index].soLuong += value;
      cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case DELETE_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index != -1) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
