import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, GET_DETAIL } from "./constant/constantShoe";

class ItemShoe extends Component {
  render() {
    let { image, name, description, price } = this.props.shoeObject;
    return (
      <div className="col-6">
        <div className="card text-center">
          <img
            style={{ width: 150, margin: "auto" }}
            className="card-img-top"
            src={image}
            alt
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">
              {description.length > 50
                ? description.slice(0, 50) + "..."
                : description}
            </p>
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.shoeObject);
              }}
              className="btn btn-warning"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.shoeObject);
              }}
              className="btn btn-danger ml-3"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleViewDetail: (shoe) => {
      let action = {
        type: GET_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
    handleAddToCart: (shoe) => {
      let action = {
        type: ADD_TO_CART,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);

// id(pin):1
// name(pin):"Adidas Prophere"
// alias(pin):"adidas-prophere"
// price(pin):350
// description(pin):"The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement. "
// shortDescription(pin):"The midsole contains 20% more Boost for an amplified Boost feeling. "
// quantity(pin):995
// image(pin):"http://svcy3.myclass.vn/images/adidas-prophere.png"
